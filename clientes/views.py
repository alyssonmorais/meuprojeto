from django.shortcuts import render, redirect
from .models import Person
from .forms import PersonForm

# Create your views here.

def persons_list(request):
    persons = Person.objects.all() #pega todos os objetos

    return render(request, 'person_list.html', {'persons': persons}) #toda função tem que retornar render()

def persons_create(request):
    form = PersonForm(request.POST or None) #cria um novo registro no BD

    if form.is_valid():
        form.save()
        return redirect('person_list') #retorna para person_list

    return render(request, 'person_create.html', {'form': form}) #person_create é o nome da URL