from django.urls import path
from .views import persons_list, persons_create #funções criadas em view


urlpatterns = [
    #link, nome da função, nome do html
    path('list/', persons_list, name='person_list'),
    path('form/', persons_create, name='person_create')
]